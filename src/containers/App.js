import React from 'react';
import {fetchAsyncGet} from '../fetch/fetchFunc';
import {siteUrl, urlParam, appID} from '../fetch/fetchURL';
import {TodayCard} from '../components/TodayCard';
import {ForecastCard} from '../components/ForecastCard';
import {Navigation} from '../components/Nav';

export class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            json: Object,
            isForecast: false,
            isReady: true,
            isError: false,
            selectedCity: '',
            positionLat: '',
            positionLon: '',
            url: '',
            isGeo: true
        };
    }
    
    componentWillMount = () => {
        this.getGeo();
    
    }

    componentDidMount = () => {
        this.setState({loading: false});
    }

    generateUrl = () => {
        console.log(this.state.positionLat)
        this.setState({
            loading: true,
            url: siteUrl + 
                (this.state.isForecast 
                    ? 'forecast?' 
                    : 'weather?') 
                + 'lat=' + this.state.positionLat 
                + '&lon=' + this.state.positionLon
                + '&' + urlParam + '&' + appID
            }, () => { 
            console.log('Current url:', this.state.url)
            this.getWeather()
        });        
    }

    getGeo = () => {
        navigator.geolocation.getCurrentPosition(position => {
        this.setState({
            positionLat: position.coords.latitude,
            positionLon: position.coords.longitude,
            isGeo: true
        }, () => {
            this.generateUrl()
        });
        })
    }

    getWeather = () => {
        fetchAsyncGet(this.state.url)
        .then((json) => {
            this.setState({
                json: json,
                selectedCity: 
                    this.state.isGeo ? 
                        json.name 
                            ? json.name 
                            : json.city.name
                        : this.state.selectedCity,
                isReady: true,
                loading: false
            });
        })
        .catch(() => {
            this.setState({
                isError: true
            })
        })
    }

    triggerForecast = () => {
        this.setState({
            isReady: false,
            isForecast: !this.state.isForecast
            }, () => {
                this.generateUrl()
                });
    };        

    updateCity = (value, lat, lon) => {
        console.log(lat, lon)
        this.setState({
            isGeo: false,
            selectedCity: value,
            positionLat: lat,
            positionLon: lon
        }, () => {
            this.generateUrl();
        });
    }

    render() {
        const {loading} = this.state;
        const {isForecast, isReady, isError} = this.state;
    
        return (
            
            <div>
                {!isError ?
                    <div>
                        <Navigation 
                            triggerForecast={this.triggerForecast} 
                            forecast={this.state.isForecast}
                            triggerGeo={this.getGeo}
                            updateCity={this.updateCity}
                            />
                    
                        {!loading ?
                            <div>
                                {
                                !(isForecast && isReady) ?
                                    <TodayCard json={this.state.json} cityName = {this.state.selectedCity}/>
                                    :
                                    <ForecastCard json={this.state.json} cityName = {this.state.selectedCity}/>
                                }
                            </div>
                            :
                            <div className = "preloader">
                                <img src = "preloader.gif" alt="preloader"/>
                            </div>
                        }
                    </div>
                    :
                    <div className="error">Connection Error, refresh page later</div>
                
                    
                }
            </div>
        );
    }
}