export const currentWeatherUrl = 'https://api.openweathermap.org/data/2.5/weather?q=Penza&units=metric&APPID=fd3f8cb24e503799fb86ea3af8bd1a8b';
export const forecastUrl = 'https://api.openweathermap.org/data/2.5/forecast?q=Penza&units=metric&APPID=fd3f8cb24e503799fb86ea3af8bd1a8b';

export const siteUrl = 'https://api.openweathermap.org/data/2.5/';
export const urlParam = 'units=metric&lang=ru';
export const appID = 'APPID=fd3f8cb24e503799fb86ea3af8bd1a8b';