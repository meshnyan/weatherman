export async function fetchAsyncGet(url) {
    console.log('Fetch url =',url,' GET has started');
    const response = await fetch(url)
    const json = await response.json()
    
    console.log('GET HTTP status: ', response.status);
    return json;
};

export async function fetchAsyncGetCity(value) {
    const url = `https://nominatim.openstreetmap.org/?addressdetails=1&q=${value}&format=json&limit=5`
    console.log('Fetch url =',url,' GET started');
    const response = await fetch(url)
    const json = await response.json()
    console.log(json)
    
    console.log('GET HTTP status: ', response.status);
    return json;
};
