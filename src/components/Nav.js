import React from 'react';
import { AutoComplete, Button, Input, Icon } from 'antd';
import 'antd/dist/antd.css';
import {fetchAsyncGetCity} from '../fetch/fetchFunc';
import '../index.css';

export class Navigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            dataSource: [],
            lat: '',
            lon: '',
        };
      }

    ForecastTrigger = (props) => {
        return (
            <Button className="btn" onClick = {this.props.triggerForecast}>
                {this.props.forecast ? "Forecast" : "Today"}
            </Button>
        );
    }

    GeoTrigger = () => {
        return (
            <Button className="btn" onClick = {this.props.triggerGeo}>
                Find my geo
            </Button>
        );
    }

    onChange = value => {
        this.setState({ value }, () => {
            fetchAsyncGetCity(this.state.value)
            .then((json) => {
                console.log('json',json)
                const arr = [];
                json.forEach(item => arr.push(`${item.display_name}`));
                this.setState({
                    dataSource: arr
                }, () => {
                    console.log('datasourse',this.state.dataSource)
                })
            })
        });
    };
 
    handleSubmit = (event) => {
        event.preventDefault();
        fetchAsyncGetCity(this.state.value)
        .then(json => {
            if(json.length !== 0) {
                this.props.updateCity(this.state.value, json[0].lat, json[0].lon);
            }
            
        })
        
    }
    render() {
        const { dataSource, value } = this.state;
        
        return (
            <div>
                <div className="div-btn">
                    {this.ForecastTrigger()}
                    {this.GeoTrigger()}
                </div>
               
                <div className="input">
                    <AutoComplete
                        value={value}
                        dataSource={dataSource}
                        style={{ width: 300 }}
                        onChange={this.onChange}
                        placeholder="begin typing"
                        />
                    <Button icon="search" onClick={this.handleSubmit}/>
                </div>
                
                
            </div>
        );
    }
}