import React from 'react';

export function TodayCard(props) {
    console.log(props.cityName)
    return (
        <div>
            {   
                (props.json.cod == 200) ?
                
                    (props.json.main && props.json.weather) ? 
                    <div className="card-div">
                        <span className="city-name">{props.cityName}</span>
                        <div className="card-border">
                            <span className="card-info">{props.json.main.temp + ' `C'}</span>
                            <span className="card-info">{props.json.weather[0].main}</span>
                            <img src={"http://openweathermap.org/img/wn/"+props.json.weather[0].icon+".png"}/>
                        </div>
                    </div>                
                    :
                    <div>
                        Not Loaded
                    </div>
                    :
                    <div className="error">
                        {props.json.message}
                    </div>
            }
        </div>
    )
}
