import React from 'react';

export class ForecastCard extends React.Component {

	//приоритеты по погодным условиям
	 
	avgIcon = (arr) => {
		const iconList = new Map([
			['01', 1],
			['02', 2],
			['03', 3],
			['04', 3],
			['09', 5],
			['10', 5],
			['11', 5],
			['13', 5],
			['50', 3],
		]);
		let temp = new Map([]);
		arr.forEach(icon => {
			if(temp.has(icon)) {
				temp.set(icon, (temp.get(icon) + iconList.get(icon)))
			}
			else {
				temp.set(icon, iconList.get(icon))
			}
		})
		let cardIcon = '';
		let counter = 0;
		temp.forEach((value, key) => {
			if (value > counter) {
				counter = value;
				cardIcon = key;
			}
		});
	 
		return cardIcon + "d";
	}
	 
	cardArray = (json) => {	
		let arr = [];
		let currentDate = 0;
		let currentMonth = 0;
		let counter = 0;
		let temp = 0;
		let currentIcon = [];
		const months = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"]
		
		json.list.forEach(elem => {
			let date = new Date(elem.dt*1000);
			if (currentDate == 0) {
				currentDate = date.getDate();
				currentMonth = date.getMonth();
			}
		
			if (currentDate != date.getDate()) {
				arr.push({date: currentDate, month: months[currentMonth], avgTemp: (temp/counter).toFixed(1), avgIcon: this.avgIcon(currentIcon)});
				currentMonth = date.getMonth();
				currentDate = date.getDate();
				counter = 0;
				temp = 0;
				currentIcon = [];
			}
		
			currentIcon.push(elem.weather[0].icon.slice(0,2));
			temp += elem.main.temp;
			counter++;
		
		});
		
		arr.push({date: currentDate, month: months[currentMonth], avgTemp: (temp/counter).toFixed(1), avgIcon: this.avgIcon(currentIcon)});
		console.log(arr)
		return arr;
	}

	cardRender = (item) => 
		<li key={item.date} className = "card-border">
			
				<span>
					{item.date}
				</span>
				<span>
					{item.month}
				</span>
				<span>
					{item.avgTemp + ' `C'}
				</span>
				<img src={"http://openweathermap.org/img/wn/"+item.avgIcon+".png"}/>
			
		</li>
		
	weatherRender = (arr) => this.cardArray(arr).map((item) => this.cardRender(item));
	
	render() {	
		return (
			<div>
				{
					(this.props.json.cod == 200) ?
					
					<div className="card-div">
						<span className="city-name">{this.props.cityName}</span>
						<ul className="forecast-ul">
							{
							this.weatherRender(this.props.json)
							}
						</ul>
					</div>
					:
					<div className="error">
                        {this.props.json.message}
                    </div>
				}
			</div>
			
		)
	}
}


